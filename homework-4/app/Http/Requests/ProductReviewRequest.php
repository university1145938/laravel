<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductReviewRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'product_id' => ['required'],
            'rating' => ['required', 'integer', 'max:5', 'min:1'],
            'comment' => ['present', 'nullable', 'string', 'max:1000']
        ];
    }
}
