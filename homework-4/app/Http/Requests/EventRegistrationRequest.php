<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRegistrationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'full_name' =>  ['required', 'string'],
            'event_id' =>  ['required'],
            'email' =>  ['required', 'email'],
            'ticket_type' =>  ['required', 'string', 'in:Regular,VIP,Speaker'],
        ];
    }
}
