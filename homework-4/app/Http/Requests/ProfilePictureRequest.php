<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfilePictureRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'picture' => ['required', 'max:2048']
        ];
    }
}
