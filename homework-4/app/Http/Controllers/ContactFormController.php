<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ContactFormController extends Controller
{
    public function store(ContactFormRequest $request)
    {
        $request = $request->validated();

        $name = $request['name'];
        $email = $request['email'];
        $message = $request['message'];

        Storage::append("./contact-forms.txt", "name: $name, email $email, message $message \n");

        return ['message' => 'success'];
    }
}
