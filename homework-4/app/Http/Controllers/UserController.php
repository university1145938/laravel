<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfilePictureRequest;
use App\Http\Requests\UserRegistrationRequest;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function register(UserRegistrationRequest $request)
    {
        $request = $request->validated();

        $name = $request['name'];
        $email = $request['email'];
        $password = $request['password'];

        Storage::append("./users.txt", "name: $name, email $email, password $password \n");

        return ['message' => 'success'];
    }

    public function storeProfilePicture(ProfilePictureRequest $request)
    {
        $filePath = $request->file('picture')->store('profile-pictures');
        $originalName = $request->file('picture')->getClientOriginalName();

        Storage::append('profile-picture-paths.txt', "path:$filePath, original name: $originalName \n");
        return ['message' => 'success'];
    }
}
