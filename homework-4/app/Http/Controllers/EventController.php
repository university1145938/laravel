<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventRegistrationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{
    public function store(EventRegistrationRequest $request)
    {
        $data = $request->Validated();

        if(!Storage::exists('events.csv')) {
            Storage::put("events.csv", 'full_name, event_id, email, ticket_type \n');
        }

        $full_name = $data['full_name'];
        $event_id = $data['event_id'];
        $email = $data['email'];
        $ticket_type = $data['ticket_type'];

        Storage::append('events.csv', "$full_name, $event_id, $email, $ticket_type" . ' \n');

        return ['message' => 'success'];
    }
}
