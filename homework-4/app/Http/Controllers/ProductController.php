<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductReviewRequest;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function storeReview(ProductReviewRequest $request)
    {
        $data = $request->Validated();

        if(!Storage::exists('reviews.json')) {
            Storage::put("reviews.json", json_encode([]));
        }


        $allReviews = (array)json_decode(Storage::get("reviews.json"));

        $productId = $data['product_id'];
        unset($data['product_id']);

        if(isset($allReviews[$productId])) {
            $allReviews[$productId][] = $data;
        } else {
            $allReviews[$productId] = [$data];
        }

        Storage::put("reviews.json", json_encode($allReviews));
        return ['message' => 'success'];
    }
}
