<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class TaskFour extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:calculate-and-write-date {days}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'calculates what date will be after x days and writes it to the log file';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $days = (int)$this->argument('days');

        Storage::append('days.log', Carbon::now()->addDays($days));
    }
}
