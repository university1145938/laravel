<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class TaskThree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:print-dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        echo Carbon::now();
        echo "\n";

        echo Carbon::now()->addMonth();
        echo "\n";

        echo Carbon::now()->diffInDays(Carbon::now()->endOfYear());
        echo "\n";
    }
}
