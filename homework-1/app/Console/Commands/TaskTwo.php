<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TaskTwo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:hello {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'prints hello with name if provided';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');
        echo "hello $name \n";
    }
}
