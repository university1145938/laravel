<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class TaskFive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:log-dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'logs current date';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Storage::append('date-time.log', now());
    }
}
