<?php

use App\Classes\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('students', function () {
    $students = Storage::read(Tenant::$storage);
    return ['data' => $students];
});

Route::post('students', function (Request $request) {
    $student = "$request->first_name $request->last_name";
    Storage::append(Tenant::$storage, $student);
    return ['message' => 'success'];
});
