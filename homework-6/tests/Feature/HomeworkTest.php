<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use function PHPUnit\Framework\assertLessThan;
use function PHPUnit\Framework\assertLessThanOrEqual;
use function PHPUnit\Framework\assertTrue;

class HomeworkTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function test_ok_status(): void
    {
        $response = $this->get('api/1');
        $response->assertStatus(200);
    }

    public function test_addition(): void
    {
        $num1 = 5;
        $num2 = 7;
        $response = $this->get("api/2?num1=5&num2=7");

        $response->assertJson(['result' => $num1 + $num2]);
    }

    public function test_car_object(): void
    {

        $response = $this->get("api/3");
        $response->assertJsonStructure(['car' => ['price', 'rating']]);
    }

    public function test_merge_sort(): void
    {

        $response = $this->get("api/9");
        $array = $response->json('array');
        for($i = 1; $i < count($array); $i++) {
            $prev = $array[$i - 1];
            $curr = $array[$i];
            assertLessThanOrEqual($curr, $prev);
        }
    }
    public function test_http_status(): void
    {

        $response = $this->post("api/10");
        $response->assertStatus(405);

        $response = $this->patch("api/10");
        $response->assertStatus(405);

    }
    public function test_missing_parameters(): void
    {

        $response = $this->post("api/12", ['title' => 'example']);
        $response->assertStatus(422);

        $response = $this->post("api/12", ['title' => 'example', 'body' => 'example']);
        $response->assertStatus(200);
    }

    public function test_content_type(): void
    {
        $response = $this->postJson("api/12", ['title' => 'example']);
        $response->assertHeader('Content-Type', 'application/json');
    }
}
