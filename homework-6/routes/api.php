<?php

use App\Classes\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('1', function () {
    return response()->json('success', 200);
});
Route::get('2', function () {
    return response()->json(['result' => (int)request()->num1 + (int)request()->num2]);
});
Route::get('3', function () {
    return response()->json(['car' => new Car(4, 5)]);
});
Route::get('9', function () {
    function merge($arr1, $arr2)
    {
        $pointer1 = 0;
        $pointer2 = 0;
        $merged = [];

        while($pointer1 < count($arr1) && $pointer2 < count($arr2)) {
            if($arr1[$pointer1] < $arr2[$pointer2]) {
                $merged[] = $arr1[$pointer1];
                $pointer1 += 1;
            } else {
                $merged[] = $arr2[$pointer2];
                $pointer2 += 1;
            }
        }


        for($i = $pointer1; $i < count($arr1); $i++) {
            $merged[] = $arr1[$i];
        }



        for($i = $pointer2; $i < count($arr2); $i++) {
            $merged[] = $arr2[$i];
        }


        return $merged;
    }

    // generating random array for merge sort
    $separated = [];
    for($i = 0; $i < 32; $i++) {
        $separated[] = [random_int(0, 1000000)];
    }

    while(count($separated) > 1) {
        $merged = [];
        for($i = 0; $i < count($separated); $i += 2) {
            $arr1 = $separated[$i];
            $arr2 = $i + 1 < count($separated) ? $separated[$i + 1] : [];
            $mergedArrays = merge($arr1, $arr2);
            $merged[] = $mergedArrays;
        }
        $separated = $merged;
    }

    return response()->json(['array' => $merged[0]]);
});

Route::get('10', function () {
    return response()->json(['message' => 'success']);
});

Route::post('12', function () {
    $validator = Validator::make(request()->all(), [
        'title' => 'required|string',
        'body' => 'required|string',
    ]);

    if($validator->fails()) {
        return response()->json($validator->messages(), 422);
    };

    return response()->json(['message' => 'success']);
});
