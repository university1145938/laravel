<?php

namespace App\Classes;

class Car
{
    public function __construct(public int $price, public int $rating) {}
}
