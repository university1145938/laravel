<?php

use App\Classes\Product;
use App\Notifications\ProductNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/products', function () {
    $product = new Product(
        request()->name,
        request()->description,
        request()->email,
        request()->url,
        request()->price,
    );

    // $product->notify(new ProductNotification($product));
    $product->sendAdvancedNotification();
    return ['message' => 'success'];
});
