<?php

namespace App\Classes;

use App\Notifications\ProductNotification;
use Illuminate\Notifications\Notifiable;

class Product
{
    use Notifiable;

    public static $priceThreshold = 10;
    public string $name;
    public string $description;
    public string $email;
    public string $url;
    public int $price;

    public function __construct(string $name, string $description, string $email, string $url, int $price)
    {
        $this->name = $name;
        $this->description = $description;
        $this->email = $email;
        $this->url = $url;
        $this->price = $price;
    }

    public function sendAdvancedNotification()
    {
        $this->notify(
            new ProductNotification($this, self::$priceThreshold)
        );
    }
}
