<?php

namespace App\Console\Commands;

use App\Classes\Product;
use Illuminate\Console\Command;

class SendRecurrentProductNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-recurrent-product-notifications {priceThreshold}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sends notifications on list oof products';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $priceThreshold = $this->argument('priceThreshold');

        Product::$priceThreshold = $priceThreshold;

        $products = [
            new Product('product 1', 'description 1', 'address1@gmail.com', 'https://amazon.com/1', 4),
            new Product('product 2', 'description 2', 'address2@gmail.com', 'https://amazon.com/2', 10)
        ];


        foreach($products as $product) {
            $product->sendAdvancedNotification();
        }
    }
}
