<?php

namespace App\Notifications;

use App\Classes\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProductNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public Product $product;
    public int $priceThreshold;

    public function __construct(Product $product, int $priceThreshold)
    {
        $this->product = $product;
        $this->priceThreshold = $priceThreshold;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        $name = $this->product->name;
        $description = $this->product->description;
        $url = $this->product->url;
        $price = $this->product->price;

        $mailMessage = new MailMessage();

        if($price < $this->priceThreshold) {
            $mailMessage->line('great deal!');
        } else {
            $mailMessage->line('usual deal');
        }

        $mailMessage->line("this is product $name with description of $description and price of $price")
                    ->action('check out this product!', "https://amazon.com/$url");

        return $mailMessage;

    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
