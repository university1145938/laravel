<?php

require "Shape.php";
require "Resizable.php";
class Circle extends Shape implements Resizable
{
    public function __construct(public int $radius) {}


    public function calculateArea(): float
    {
        return $this->radius * $this->radius * 3.14;
    }

    public function resize(int $unit)
    {
        $this->radius += $unit;
    }
}
