<?php

interface Resizable
{
    public function resize(int $unit);
}
