<?php

require "Person.php";

// student
class Student extends Person
{
    public function __construct(string $name, int $age, protected int $studentId)
    {
        parent::__construct($name, $age);
    }

    public function getAge(): int
    {
        return $this->studentId;
    }

    public function getStudentId(): int
    {
        return $this->studentId;
    }
}

$student = new Student("student name", 20, 77);


echo $student->getProperties();
echo "\n";

echo $student->getAge();
echo "\n";


echo $student->getStudentId();
echo "\n";
