<?php

require "Shape.php";
require "Resizable.php";

class Rectangle extends Shape implements Resizable
{
    public function __construct(public int $width, public int $height) {}

    public function calculateArea(): float
    {
        return $this->width * $this->height;
    }

    public function resize(int $unit)
    {
        $this->width += $unit;
        $this->height += $unit;
    }
}
