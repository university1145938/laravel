<?php

require "Logger.php";

class Person
{
    use Logger;

    public static array $personClassInstances = [];

    public function __construct(public string $name, private int $age)
    {
        self::$personClassInstances[] = $this;
        self::log("person $name was created");
    }

    public static function averageAge(): int
    {
        $total = 0;
        foreach(self::$personClassInstances as $person) {
            $total += $person->age;
        }

        return $total / count(self::$personClassInstances);
    }

    public function getProperties(): string
    {
        return "$this->name $this->age";
    }

    public function getAge(): int
    {
        return $this->age;
    }
}

$person = new Person("name", 20);

echo $person->getProperties();
echo "\n";


new Person("name1", 30);
new Person("name2", 60);

echo "average age is: " . Person::averageAge();
echo "\n";


echo $person->getAge();
echo "\n";
