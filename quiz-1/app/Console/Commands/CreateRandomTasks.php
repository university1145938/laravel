<?php

namespace App\Console\Commands;

use App\Classes\Task;
use Faker\Factory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CreateRandomTasks extends Command
{
    protected $signature = 'app:create-random-tasks {numberOfTasks}';
    protected $description = 'creates random tasks and logs them into a file';


    public function handle()
    {
        $numOfTasks = (int)$this->argument('numberOfTasks');
        $faker = Factory::create();
        $tasks = [];



        for($i = 0; $i < $numOfTasks; $i ++) {
            $task = new Task(
                $faker->title(),
                $faker->sentence(),
                $faker->date('Y-m-d'),
                $faker->date('Y-m-d'),
            );

            $tasks[] = $task;

            Storage::append('todos.log', $task->getInfo() . "\n");
        }

        return $tasks;

    }
}
