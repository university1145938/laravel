<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTaskRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required', 'string'],
            'description' => ['required', 'string'],
            'plannedFinishDate' => ['required', 'date'],
            'finishDate' => ['required', 'date'],
            'status' => ['nullable', 'boolean']
        ];
    }
}
