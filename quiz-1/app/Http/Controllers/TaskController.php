<?php

namespace App\Http\Controllers;

use App\Classes\Task;
use App\Classes\TaskList;
use App\Http\Requests\CreateTaskListRequest;
use App\Http\Requests\CreateTaskRequest;
use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class TaskController extends Controller
{
    public function create(CreateTaskRequest $request)
    {
        $request = $request->validated();

        $task = new Task(
            $request['title'],
            $request['description'],
            $request['plannedFinishDate'],
            $request['finishDate'],
            $request['status'] ?? false
        );

        return ['task' => $task->getInfo()];
    }

    public function createMany(CreateTaskListRequest $request, int $count)
    {
        $request = $request->validated();

        $taskList = new TaskList(
            $request['title'],
            $request['description'],
        );

        $numOfTasks = $count;
        $faker = Factory::create();

        for($i = 0; $i < $numOfTasks; $i ++) {
            $task = new Task(
                $faker->title(),
                $faker->sentence(),
                $faker->date('Y-m-d'),
                $faker->date('Y-m-d'),
            );

            $taskList->addTask($task);
        }

        $taskInfos = [];

        foreach($taskList->getAllTasks() as $task) {
            $taskInfos[] = $task->getInfo();
        }

        return ['tasks' => $taskInfos];
    }
}
