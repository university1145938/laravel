<?php

namespace App\Http\Controllers;

use App\Classes\Task;
use Faker\Factory;
use Illuminate\Http\Request;

class CreateFinishedTask extends Controller
{
    public function __invoke()
    {

        $faker = Factory::create();
        $task = new Task(
            $faker->title(),
            $faker->sentence(),
            $faker->date('Y-m-d'),
            $faker->date('Y-m-d'),
        );

        $task->finish();

        return [
            "title" => $task->getTitle(),
            "finishDate" => $task->getFinishDate()
        ];

    }
}
