<?php

namespace App\Classes;

class Task implements TaskInterface
{
    use TaskTrait;
    protected string $title;
    protected string $description;
    protected string $plannedFinishDate;
    protected string $finishDate;
    protected bool $status;


    public function __construct(
        string $title,
        string $description,
        string $plannedFinishDate,
        string $finishDate,
        bool $status = false,
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->plannedFinishDate = $plannedFinishDate;
        $this->finishDate = $finishDate;
        $this->status = $status;
    }
}
