<?php

namespace App\Classes;

interface TaskInterface
{
    public function getTitle(): string;
    public function getDescription(): string;
    public function isFinished(): bool;
    public function getPlannedFinishDate(): string;
    public function finish(): void;
    public function getFinishDate(): string;
    public function getInfo(): string;
}
