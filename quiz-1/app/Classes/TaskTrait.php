<?php

namespace App\Classes;

trait TaskTrait
{
    public function getTitle(): string
    {
        return $this->title;
    }
    public function getDescription(): string
    {
        return $this->description;
    }
    public function isFinished(): bool
    {
        return $this->status;
    }
    public function getPlannedFinishDate(): string
    {
        return $this->plannedFinishDate;
    }
    public function finish(): void
    {
        if(!$this->status) {
            $this->finishDate = now()->format('Y-m-d');
            $this->status = true;
        } else {
            echo "task is already finished";
        }
    }
    public function getFinishDate(): string
    {
        return $this->finishDate;
    }
    public function getInfo(): string
    {
        $info = "title: $this->title, description: $this->description, plannedFinishDate: $this->plannedFinishDate, 
        finishDate: $this->finishDate, status: $this->status";

        return $info;
    }
}
