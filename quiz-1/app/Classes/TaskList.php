<?php

namespace App\Classes;

class TaskList
{
    protected $taskList = [];
    protected string $title;
    protected string $description;

    public function __construct(string $title, string $description)
    {
        $this->title = $title;
        $this->description = $description;
    }

    public function addTask(Task $task): void
    {
        $this->taskList[] = $task;
    }

    public function getAllTasks(): array
    {
        return $this->taskList;
    }
}
