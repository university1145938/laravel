<?php

use App\Http\Controllers\CreateFinishedTask;
use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/



Route::prefix('task')->group(function () {
    Route::post('/create', [TaskController::class, 'create']);
    Route::post('/create-many/{count}', [TaskController::class, 'createMany']);
    Route::post('/create-finished-task', CreateFinishedTask::class);
});
